package transport

import (
	"context"
	tokenservice "tokenserver/proto"
)

func DecodeGet (ctx context.Context, req interface{}) (request interface{}, err error) {
	if req == nil {
		return nil , err
	}
	return req.(*tokenservice.ReqGetToken) , nil
}

func EncodeGet (ctx context.Context, rep interface{}) (response interface{}, err error) {
	if rep == nil {
		return nil , err
	}
	return rep.(*tokenservice.RepGetToken) , nil
}

func DecodeIsVal (ctx context.Context, req interface{}) (request interface{}, err error) {
	if req == nil {
		return nil , err
	}
	return req.(*tokenservice.IsValidTokenReq) , nil
}

func EncodeIsVal (ctx context.Context, rep interface{}) (response interface{}, err error) {
	if rep == nil {
		return nil , err
	}
	return rep.(*tokenservice.IsValidToeknRep) , nil
}