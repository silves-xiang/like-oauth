module tokenserver

go 1.14

require (
	github.com/garyburd/redigo v1.6.2
	github.com/go-kit/kit v0.10.0
	github.com/golang/protobuf v1.4.2
	github.com/jinzhu/gorm v1.9.16
	golang.org/x/net v0.0.0-20210405180319-a5a99cb37ef4 // indirect
	golang.org/x/sys v0.0.0-20210403161142-5e06dd20ab57 // indirect
	google.golang.org/grpc v1.37.0
	google.golang.org/protobuf v1.25.0
)
