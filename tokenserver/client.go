package main

import (
	"context"
	"fmt"
	"google.golang.org/grpc"
	"tokenserver/bproto"
	"log"
	tokenservice "tokenserver/proto"
)

func U1 () string {
	con , err := grpc.Dial(":8081" , grpc.WithInsecure())
	if err != nil {
		log.Println("connect is failed")
	}
	client := codeserver.NewCodeServerClient(con)
	rep , err := client.GetCode(context.Background() , &codeserver.GetCodeReuqest{UserId: "xiang" , Client: "tengxun"})
	fmt.Println(rep)
	return rep.GetCode()
}

func U2(code string ) {
	con , err := grpc.Dial(":8082" , grpc.WithInsecure())
	if err != nil {
		log.Println(err)
		return
	}
	client := tokenservice.NewTokenServicesClient(con)
	rep , err := client.GetToken(context.Background() , &tokenservice.ReqGetToken{Secret: "woshixiangbo" , ClientId: "1", Code: code})
	if err != nil {
		log.Println(err)
		return
	}
	fmt.Println(rep)
}

func main () {
	code := U1()
	U2(code)
}