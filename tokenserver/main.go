package main

import (
	"google.golang.org/grpc"
	"log"
	"net"
	"tokenserver/endpoint"
	"tokenserver/proto"
	"tokenserver/router"
	"tokenserver/services"
)

func main () {
	svc := services.ServiceAI{}
	end := endpoint.ServiceEndpoint{
		GetT: endpoint.MakeGetEndpoint(svc),
		IsVal: endpoint.MakeIsValToken(svc),
	}
	r := router.NewRouter(end)
	lis , err := net.Listen("tcp" , ":8082")
	gserver := grpc.NewServer()
	if err != nil {
		log.Println(err)
		return
	}
	tokenservice.RegisterTokenServicesServer(gserver , r)
	err = gserver.Serve(lis)
	if err != nil {
		log.Println(err)
	}
}