package endpoint

import (
	"errors"
	"github.com/go-kit/kit/endpoint"
	tokenservice "tokenserver/proto"
	"context"
)

type ServiceEndpoint struct {
	GetT endpoint.Endpoint
	IsVal endpoint.Endpoint
}

func MakeGetEndpoint (svc tokenservice.TokenServicesServer) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		if request == nil {
			return nil , errors.New("the request is nil")
		}
		req := request.(*tokenservice.ReqGetToken)
		rep , err := svc.GetToken(ctx , req)
		if rep == nil {
			return nil , err
		}
		return rep , err
	}
}

func MakeIsValToken (svc tokenservice.TokenServicesServer) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		if request == nil {
			return nil , errors.New("request is nil")
		}
		req := request.(*tokenservice.IsValidTokenReq)
		rep , err := svc.IsValidToken(ctx , req)
		if rep == nil {
			return nil , err
		}
		return rep , nil
	}
}