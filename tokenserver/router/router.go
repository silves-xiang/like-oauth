package router

import (
	"context"
	"github.com/go-kit/kit/transport/grpc"
	tokenservice "tokenserver/proto"
	"tokenserver/endpoint"
	"tokenserver/transport"
)

type ServicesAI struct {
	GetT grpc.Handler
	IsVT grpc.Handler
}

func (s ServicesAI) GetToken (ctx context.Context, req *tokenservice.ReqGetToken) (*tokenservice.RepGetToken, error) {
	_ , rep , err := s.GetT.ServeGRPC(ctx , req)
	if rep == nil {
		return nil , err
	}
	response := rep.(*tokenservice.RepGetToken)
	return response , err
}

func (s ServicesAI) IsValidToken(ctx context.Context, req *tokenservice.IsValidTokenReq) (*tokenservice.IsValidToeknRep, error) {
	_ , rep , err := s.IsVT.ServeGRPC(ctx , req)
	if rep == nil {
		return nil , err
	}
	response := rep.(*tokenservice.IsValidToeknRep)
	return response , err
}

func NewRouter (end endpoint.ServiceEndpoint) tokenservice.TokenServicesServer {
	return ServicesAI{
		GetT: grpc.NewServer(
			end.GetT ,
			transport.DecodeGet ,
			transport.EncodeGet ,
			),
		IsVT:  grpc.NewServer(
			end.IsVal ,
			transport.DecodeIsVal ,
			transport.EncodeIsVal ,
			),
	}
}