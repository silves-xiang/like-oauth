package transport

import (
	"context"
	userauth "userauthserver/proto"
)

func DecodeAuth (ctx context.Context, req interface{}) (request interface{}, err error) {
	if req == nil {
		return nil , err
	}
	return req.(*userauth.AuthRequest) , nil
}

func EncodeAuth (ctx context.Context, rep interface{}) (response interface{}, err error) {
	if rep == nil {
		return  nil , err
	}
	return rep.(*userauth.AuthResponse) , nil
}
