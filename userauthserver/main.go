package main

import (
	"google.golang.org/grpc"
	"log"
	"net"
	"userauthserver/endpoint"
	userauth "userauthserver/proto"
	"userauthserver/router"
	"userauthserver/services"
)

func main () {
	svc := services.AuthServicesA{}
	end := endpoint.UserEndA{
		UserE: endpoint.MakeUEndpoint(svc),
	}
	r := router.NewRouter(end)
	lis , err := net.Listen("tcp" , ":8083")
	if err != nil {
		log.Println(err)
		return
	}
	gserver := grpc.NewServer()
	userauth.RegisterAuthServer(gserver , r)
	gserver.Serve(lis)
}