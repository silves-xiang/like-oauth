package router

import (
	"context"
	"errors"
	"github.com/go-kit/kit/transport/grpc"
	"userauthserver/endpoint"
	"userauthserver/proto"
	"userauthserver/transport"
)

type UserAuthA struct {
	UserA grpc.Handler
}

func (u UserAuthA) AuthT(ctx context.Context, req *userauth.AuthRequest) (*userauth.AuthResponse, error) {
	_ , rep , err := u.UserA.ServeGRPC(ctx , req)
	if err != nil {
		return nil , err
	}
	if rep == nil {
		return nil , errors.New("the response is nil")
	}
	return rep.(*userauth.AuthResponse) , nil
}

func NewRouter (end endpoint.UserEndA) userauth.AuthServer {
	return &UserAuthA{
		UserA: grpc.NewServer(
			end.UserE,
			transport.DecodeAuth,
			transport.EncodeAuth,
			),
	}
}