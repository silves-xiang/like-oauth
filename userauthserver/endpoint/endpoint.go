package endpoint

import (
	"context"
	"errors"
	"github.com/go-kit/kit/endpoint"
	userauth "userauthserver/proto"
)

type UserEndA struct {
	UserE endpoint.Endpoint
}

func MakeUEndpoint (svc userauth.AuthServer) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		if request == nil {
			return nil , errors.New("the request is nil")
		}
		req := request.(*userauth.AuthRequest)
		rep , err := svc.AuthT(ctx , req)
		if rep == nil {
			return nil , errors.New("the response is nil")
		}
		return rep , nil
	}
}