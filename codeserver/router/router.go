package router

import (
	"codeserver/enpoint"
	"context"
	"codeserver/proto"
	"errors"
	"github.com/go-kit/kit/transport/grpc"
	"codeserver/transport"
)

type ServicesA struct {
	Get grpc.Handler
	Isvali grpc.Handler
}

func (s ServicesA) GetCode(ctx context.Context, req *codeserver.GetCodeReuqest) (*codeserver.RCodeResponse, error) {
	_ , rep , err := s.Get.ServeGRPC(ctx , req)
	if err != nil {
		return nil , err
	}
	if rep == nil {
		return nil , errors.New("the response is nil")
	}
	return rep.(*codeserver.RCodeResponse) , err
}

func (s ServicesA) Isvalid (ctx context.Context, req *codeserver.ValidRequest) (*codeserver.ValidResponse, error) {
	_ , rep , err := s.Isvali.ServeGRPC(ctx , req)
	if err != nil {
		return nil , err
	}
	if rep == nil {
		return nil, errors.New("the response is nil")
	}
	return rep.(*codeserver.ValidResponse) , nil
}

func NewRouter (endpoints enpoint.EndA) codeserver.CodeServerServer {
	return &ServicesA{
		Get: grpc.NewServer(
			endpoints.GetEndpoint ,
			transport.DecodeGet ,
			transport.EncodeGet ,
			) ,
		Isvali: grpc.NewServer(
			endpoints.IsValidEndpoint ,
			transport.DecodeValid ,
			transport.EncodeValid ,
			),
	}
}