package main

import (
	"codeserver/router"
	"codeserver/services"
	"codeserver/enpoint"
	"codeserver/proto"
	"google.golang.org/grpc"
	"log"
	"net"
)

func main () {
	svc := services.ServicesA{}
	endpoin := enpoint.EndA{GetEndpoint: enpoint.MakeGetEndpoint(svc) , IsValidEndpoint: enpoint.MakeValidEndpoint(svc)}
	r := router.NewRouter(endpoin)
	lis , err := net.Listen("tcp" , ":8081")
	grpcserver := grpc.NewServer()
	if err != nil {
		log.Println(err)
		return
	}
	codeserver.RegisterCodeServerServer(grpcserver , r)
	grpcserver.Serve(lis)
}