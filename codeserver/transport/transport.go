package transport

import (
	codeserver "codeserver/proto"
	"context"
	"errors"
)

func DecodeGet (c context.Context, req interface{}) (request interface{}, err error) {
	if req == nil {
		return nil , errors.New("the trquest is nil")
	}
	request = req.(*codeserver.GetCodeReuqest)
	return
}

func EncodeGet (c context.Context, rep interface{}) (response interface{}, err error) {
	if rep == nil {
		return nil ,errors.New("the response is nil")
	}
	response = rep.(*codeserver.RCodeResponse)
	return
}

func DecodeValid (c context.Context, req interface{}) (request interface{}, err error) {
	if req == nil {
		return nil , errors.New("the request is nil ")
	}
	request = req.(*codeserver.ValidRequest)
	return
}

func EncodeValid (c context.Context, rep interface{}) (response interface{}, err error) {
	if rep == nil {
		return nil , err
	}
	response = rep.(*codeserver.ValidResponse)
	return
}
