package modules

type Request struct {
	Client string
	UserId int
}

type Response struct {
	Code string
}