package main

import (
	"context"
	"fmt"
	"google.golang.org/grpc"
	"log"
	"codeserver/proto"
)

func main () {
	client , err := grpc.Dial("127.0.0.1:8081" , grpc.WithInsecure())
	if err != nil {
		log.Println(err)
		return
	}
	client_server := codeserver.NewCodeServerClient(client)
	rep , err := client_server.GetCode(context.Background() , &codeserver.GetCodeReuqest{UserId: "1015286189" , Client: "tencent"})
	fmt.Println(rep)
	re , err := client_server.Isvalid(context.Background() , &codeserver.ValidRequest{Code: rep.Code , UserId: "1015286189"})
	if err != nil {
		log.Println(err)
	}
	fmt.Println(re.IsValid)
}