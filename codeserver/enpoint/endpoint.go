package enpoint

import (
	codeserver "codeserver/proto"
	"errors"
	"github.com/go-kit/kit/endpoint"
	"context"
)

type EndA struct {
	GetEndpoint endpoint.Endpoint
	IsValidEndpoint endpoint.Endpoint
}

func MakeGetEndpoint (svc codeserver.CodeServerServer) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		if request == nil {
			return nil , errors.New("the request is nil ")
		}
		req := request.(*codeserver.GetCodeReuqest)
		rep , err := svc.GetCode(ctx , req)
		if rep == nil {
			return nil , errors.New("the response is nil")
		}
		return rep , nil
	}
}


func MakeValidEndpoint (svc codeserver.CodeServerServer) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		if request == nil {
			return nil , errors.New("the request is nil")
		}
		req := request.(*codeserver.ValidRequest)
		response , err = svc.Isvalid(ctx , req)
		if err != nil {
			return nil , err
		}
		if response == nil {
			return nil , errors.New("the response is nil")
		}
		return response , nil
	}
}
